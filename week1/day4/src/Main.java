import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) { // psvm!
        // keskeseks teemaks ARRAYS ehk massiiv
        int[] massiiv = new int[6]; // primitiivsetest koosnev, vähem paindlik. Tema pikkus on lõplil!!
        ArrayList list = new ArrayList(); // objektisdest koosnev. See on objekt, seda on vaja kuskilt tuua
        // pikkus pole oluline

        // ÜL: massivi sisse numbrid
        String massiivStr = Arrays.toString(massiiv);  // staatiline massiiv, vaja teha string et välja printida
        System.out.println(massiivStr);

        //määra kolmas number massiivis 5
        massiiv[2] = 5; // 3. väärtuse määran 5
        System.out.println(Arrays.toString(massiiv)); // TERVE massiiv

        // prindi välja kuures ja määra
        massiiv[5] = 13;
        System.out.println(massiiv[5]); // ta oskab inte välja printida

        //prindi välja viimane element ükskõik kui pikk masiiv
        int viimane = massiiv[massiiv.length - 1]; //saan viimase numbri kätte
        System.out.println("Viimane number on " + viimane);
        // loo uus massiiv, kus on 8 numbrit
        int[] massiiv2 = new int[]{1, 2, 3, 4, 5, 6, 7, 8};  // kirjutan numbrid
        String massiivStr2 = Arrays.toString(massiiv2);
        System.out.println(Arrays.toString(massiiv2));
        // prindi välja ükshaaval kõik väärtused massiiv2-st
        // teeme uue tsükli et käia iga numbri juures ära. määrame ära mitmes meie index on
        int index = 0;
        while (index < massiiv2.length) {
            System.out.println(massiiv2[index]);
            index++; // suurene ühe võrra; nüüd on index=1, mis on teisel kohal?
        }
        //  teeme selle sama tsükli kasutades FOR
        for (int järg = 0; järg < massiiv2.length; järg++) {
            System.out.println(massiiv2[järg]);
        }

        // loo Stringide array, mis on tühi, aga siis lisad veel keskel stringi

        String[] hulk = new String[3];
        hulk[1] = "Hopsaaa";
        System.out.println(Arrays.toString(hulk));

        // loo massiiv kus 100 kohta
        // sisesta sellesse loetelu


        int[] jrk = new int[100];
        for (int i = 0; i < jrk.length; i++) {
            jrk[i] = i;

        }
        System.out.println(Arrays.toString(jrk));

        // 1. kasuta jrk massiivi, kus on nr jada
        // 2. loe mitu paaris arvu on

        int Mitupaarisarvu = 0;
        for (int i = 0; i < jrk.length; i++) {
            if (i % 2 == 0) {
                Mitupaarisarvu++;
            }
        }
        ArrayList list2 = new ArrayList();
        list2.add(4);
        list2.add(5);
        list2.add("boo");
        list2.add("jee");
        System.out.println(list2.get(2)); // prindin välja listist 3. väärtuse
        System.out.println(list2);
        for (int i = 0; i <list2.size() ; i++) { //prindin välja väärtused eraldi list2.size() on listi suurus
            System.out.println(list2.get(i));

        }

        // loo uus array list kus on 543 nr
        // numbrid suvalised Math.random() vahemikus 0-10
        //kooruta iga nr 5ga
        //salvesta uus number uuele positsioonile

        ArrayList massiiv543 = new ArrayList (); // võib panna ArrayList<Integer> tüüp objekt et sinna saab ainult inte panna
                for (int i=0; i <543; i++){
                    int nr = (int) (Math.random() *11); //random võtab suvalise
                    massiiv543.add(nr);

                }

        System.out.println("Algne massiiv " + massiiv543);
        for (int i = 0; i <massiiv543.size() ; i++) {//stringil on length, massiivil on size
            int nr =(int) massiiv543.get(i); // castime intiks, sest me teame mis seal arraysd
            int muudetudNr = nr * 5; // korrutame viiega
            massiiv543.set(i, muudetudNr); // i= element ja muudetudnr = objekt

        }

        System.out.println("Uus massiiv " + massiiv543);

    }




}



