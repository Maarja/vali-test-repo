import java.util.ArrayList;

public class Võistlus {
    ArrayList <Suusataja> voistlejad;
    int Distants;

    public Võistlus() {
        System.out.println("Start");
        voistlejad = new ArrayList();
        Distants = 20;
        for (int i = 0; i <50 ; i++) {
            int voistleja = i;
            voistlejad.add (new Suusataja(i));

        }
        System.out.println(voistlejad);
        aeg();
    } // arg käivitub, ütleb suusatajale et liigu edasi, siis ootab 1sek, siis käivitab uuesti

    public void aeg() {
        System.out.println(voistlejad);
        for (Suusataja s: voistlejad) { // käime iga suusataja juurest läbi ja ütleme et suusata
            s.suusata(); // s on konkreetne suusataja
            boolean lopetanud = s.kasOnLopetanud (Distants);
                if(lopetanud){
                    System.out.println("Võitja on " + s);
                    return;
                }

        }
        try {
            Thread.sleep (10); // thread on üks mõttejoon. Pane see praegune ülal olev käsklus magama 1 sek
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        aeg();
    }

}
