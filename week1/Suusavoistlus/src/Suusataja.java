public class Suusataja
{
    int stardiNr;
    double kiirus;
    double läbitudDistants;
    double dopinguKordaja; //1. 1/10 liiguvad kiiremini
                            // 2. 0 kiirusega ei tohiks keegi sõita
                             // 3. keegi sai vigastada ja katkestab (randomilt)
        // 4. joonista võistlus välja

    public Suusataja (int i) {
        stardiNr = i;
        kiirus = Math.random()*20;// 20 km/h
        läbitudDistants = 0;
    }
    public void suusata() {
        läbitudDistants += kiirus/3600 ; // jagan kilomeetrit sekundis
    }
    public String toString () {  // kui tahame array listi välja printida
        int dist = (int) (läbitudDistants *1000);  //
        return stardiNr + ": " + dist;


    }

    public boolean kasOnLopetanud(int Distants) {
        return läbitudDistants >= Distants;
    }
}
