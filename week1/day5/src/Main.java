import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static <Hashmap> void main(String[] args) {
        System.out.println("Tere!");

        //loo 3 muutjuat numbritega
        // moodusta lause nende muutujatega
        // prindi välja

        int aasta = 88;
        int kuu = 9;
        int paev = 16;
        String lause = "Ma sündisin aastal" + aasta + ". Kuu oli" + kuu + " ja päev oli" + paev + ".";
        System.out.println(lause);
        // %d on täisarv
        // %f on komaga
        // %s on string
        String parem = String.format("Ma sündisin aastal %d. Kuu oli %d ja aasta %d. ", aasta, kuu, paev);
        System.out.println(parem); //String Format: lauses õigele kohale

        //1.Loe mitu lampi on klassis ja pane see hashmappi
        // ,mitu akent on klassis ja tee sama
        // mitu inimest on klassis ja loe kokku

        HashMap klassiAsjad = new HashMap();
        klassiAsjad.put("aknad", 5);
        klassiAsjad.put("lampe", 11);   // ei saa teha kahte sama väärtusega võtit, nt kui kaks lampi siis kirjutab ühe üle
        klassiAsjad.put("inimesi", 21);
        System.out.println(klassiAsjad);
        int inimesi = (int) klassiAsjad.get("inimesi");
        System.out.println(klassiAsjad.get("inimesi")); // prindin võtme
        //lisa mitu tasapinda on klassis, nr enne ja siis string nt 10 ja tasapinnad
        klassiAsjad.put(10, "tasapinnad");
        System.out.println(klassiAsjad);

        System.out.println(klassiAsjad.get("tasapinnad")); // mika null? int!

        // loo uus hashm

        //järjekord tuleb sellest kuidas sattus mällu

        // Prindi välja kui palju on inimesi kasutades

        // uus hashmap kuhu saab lisada ainult sring: double paare. sisesta sinna miskit
        HashMap<String, Double> Erinevad = new HashMap<>(); //<määran ära mis lährb dsisse
        Erinevad.put("miskinr", 7567.99);
        System.out.println(Erinevad);

        //SWITCH - kuhu rong jõuab?
        int ronginumber = 50;
        String suund = null;
        switch (ronginumber) {
            case 50:
                suund = "Pärnu";
                break;
            case 55:
                suund = "Haapsalu";
                break;
            case 10:
                suund = "Vormsi";
                break;

        }
        System.out.println(suund);

        //FOREACH

        int[] mingindNr = new int[]{8, 5555, 54545454, 42342};
        for (int i = 0; i < mingindNr.length; i++) {
            System.out.println(mingindNr[i]);
        }
        System.out.println("------------------------------");

        // lihtsam viis kuidas sama asja teha


        for (int nr: mingindNr) {  // prindi need asjad
            System.out.println(nr);

        }

        // Õpilanje saab tööst punkte 0-100
        // kui punkte alla 50, kukub läbi
        // vastasel juhuk on hnne  täisarvuline punktid/20
        // 100 punkte => 5
        // 80 punkti on => 4
        // 67 punkte on => 3
        // 50 punkte = > 2
        int punkte = 99;
        if (punkte>100 || punkte <0) {
            throw new Error();

        }
                switch((int) Math.round(punkte/20.0)) {
                    case 5:
                        System.out.println("super");
                        break;
                    case 4:
                        System.out.println("hea");
                        break;
                    case 3:
                        System.out.println("rahuldav");
                        break;
                    case 2:
                        System.out.println("eee");
                        break;
                    default: // kui ükski ei matchi
                        System.out.println("HAAAA");

                }




    }
}
