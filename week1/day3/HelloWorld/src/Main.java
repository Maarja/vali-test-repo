public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World!"); //(String[] args) on see mis sisse võtab
        System.out.println("sout");// sout on kiirkäesk
        int number1 = 5;
        double number2 = 5.1; // primitiiv tüüpi objekt
        byte number3 = 5; // väärtused -128 kuni 127
        String nimi = "Krister"; //insdantsid on mälus keerukamad ja järavad meelde nt mitu tähemärki
        char AlgusTaht = 'K'; //string on kahekordse jutumärgi ja char on ühekordse jutumärgiga
        if (number1 == 5) {
        } else { }
        String liitmine2 = "tere " + 5;
        System.out.println(liitmine2);
        int liitmine = (int) (number1 + number2); //castimine kui (int) ette panen
        System.out.println(liitmine);
        String puuvli1 = "Banaan";
        String puuvli2 = "Banaan";
        if (puuvli1 == puuvli2) {
            System.out.println("puuvlid on võrdsed");
        } else {
            System.out.println("ei ole võrdsed"); }
        Koer.auh(); // koer on KLASS ja AUH on FUNKTSIOON
        Kass.nurr();
        Koer.lausu();
    }
    public static int summa(int a, int b) { // peab ära märkima et a ja b on int
        return a + b;
    }
}

