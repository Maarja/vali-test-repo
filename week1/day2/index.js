console.log("hommik")
/* muutuja nt 7, see on sisend
1) kui vähem kui 7
2) siis x2 kui väiksem kui 7,jaga kahega
3) kui == 7 jäta sama
*/
var sisend = 7 
var tulemus

if (sisend == 7) {
	tulemus = sisend
} else if (sisend < 7) {
 	tulemus = sisend * 2 
} else {
	tulemus = sisend / 2
}
console.log("Tulemus on: " + tulemus)

/* kui sõned on võrdsed, siis prindi 1
kui erinevad, siis liida kogu ja prindi

*/

var str1 = "banaan"
var str2 = "apelsin"

if (str1 == str2) {
console.log ("1")
} else
console.log (str1 + " " + str2)


/* 
antud linnade nimekiri, aga ilma sõnata "linn"
need palun lisada

JS  list [] sulgudes!

tsükkel> defineewrin algoritmi, mille defineerin iga väärtuse jaoks eraldi
*/

var linnad = ["Tallinn", "Tartu", "Valga"]
var uuedLinnad = [] // teen uue hulga

while (linnad.length > 0) { // nii kaua kui on nimekirjas linnu
 var linn = linnad.pop()  // võta välja viimane
 var uusLinn = linn + "linn" // lisa linn
 uuedLinnad.push(uusLinn) // tulemus salvesta uude listi
}
console.log(uuedLinnad)

/* eralda poiste ja tüdrukute nimed

*/
var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

// kui poisi nimi, lisa poiste lisiti; kui tydruk> tydrukute listi
// "a" lopuga on tydrukuNimi
while (nimed.length > 0) {
	var nimi = nimed.pop()
	var viimaneTaht = nimi.endsWith()
if (nimi.endsWith ("a")) {
	tydrukuteNimed.push(nimi) //pushin nime sest loopi sees var

} else { poisteNimed.push(nimi)


}
}
console.log(poisteNimed, tydrukuteNimed)

// ctrl + klikk saad mitu kursorit
// crtl + enter tekitad uue rea
// console.log ("muutuja", muutuja) prindib lähe info

//* FUNKTSIOONID *//
/* kirjuta algoritm, mis suudab ükskõik millist naise/mehe nime eristada

*/

var eristaja = function (nimi) { // () võtab vastu muutuja Input=nimi
	if (nimi.endsWith("a")) {
		return "tydruk"
	} else {
		return "poiss"
	}
}
var praeguneNimi = "Peeter"
var kumb = eristaja (praeguneNimi) // see on fuinktsioon, tuleb käivitada
console.log(kumb)

/*
loo func, mis tagastab vastuse: kas tegu on numbriga
!isNan(4) hüüumärk vastandav
*/

var kasOnNumber = function(number){
	if (!isNaN(number)) {
		return true
	} 
	return false

}
console.log( kasOnNumber (4) )
console.log( kasOnNumber ("mingi sõne") )
console.log( kasOnNumber (2356) )
console.log( kasOnNumber (6.876) )
console.log( kasOnNumber (null) )
console.log( kasOnNumber ([1, 4, 5, 6]) )

/* kirjuta funktsiion, mis võtab vastu 2 numbrit ja tagastab nende summa */

 var summa = function(a, b) {
	return a + b // funktsiooni sees on a ja b, need kirjutan siia
 }

console.log(summa(4, 5)) // summa=summa funktsioon, selle pärast käivitub
console.log(summa(7, 87))

/* JS-s on võti:väärtus

*/

var inimesed = {
	"kaarel": 34, 
	"Margarita": 10,
	"Suksu": 5,
	"Krister": 5
}

console.log(inimesed.kaarel)
console.log(inimesed.Krister)     